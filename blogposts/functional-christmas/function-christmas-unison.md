# Functional Christmas

First I have to choose a topic. Let's choose one I know nothing about,
try to figure out what it is, why I need it, and write that down.

Or, if I think it's crap, then I'll try to explain in my opinion why...
(if that happens then I probably didn't understand it)


ok....I choooooooose....... (looking through the trello board)

<b>Unison</b>


I would like to use an example to show of what I will (hopefully) learn
later today.
Instead of just rewriting the docs. 
Let's hope I will be able to do that...

Per Øyvind posted a link, 
checking <a href="https://www.unisonweb.org/">that</a> out.
Found a 
<a href="https://www.youtube.com/watch?v=gCWtkvDQ2ZI">youtube</a> 
movie of the guy that's making it. Watched about 9 minutes...

ok, time to dive in.

<a href="https://www.unisonweb.org/docs/quickstart">
Three-minute quickstart guide.
</a>

don't think they included the time it took to join the slack channel
in order to find the installation instructions,
run `brew tap unisonweb/unison`
with apparently a very slow internett connection, 
opening spotify, spotify crashing, reopening spotify, finding something 
to listen to, 
checking how to get 
<a href="https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#code">
inline backticks
</a> 
right so that I could write this.



<b>Got it! Done.</b>
![alt text1][img-3-minutes-complete]
only took me about an hour... aparently I've sorted a list of numbers.
Don't know how of course.



[img-3-minutes-complete]: img/3-minutes-completed.png "Three-minute quickstart complete!"

